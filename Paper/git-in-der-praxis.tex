\documentclass{lni}
\usepackage{url}
\usepackage{hyperref}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{here}

\author{
	Bengt Albert Lüers \\
	Bengt.Lueers@gmail.com \\
	\\
	Department für Informatik \\
	Fakultät II - Informatik, Wirtschafts- und Rechtswissenschaften \\
	Carl von Ossietzky Universität Oldenburg \\
	Ammerländer Heerstr. 114-118 \\
	26129 Oldenburg
}
\title{Git in der Praxis}
\begin{document}
\maketitle

\begin{abstract}
Git ist ein modernes Versionskontrollsystem  mit relativ großer Verbreitung und Potential, Entwicklungsprozesse zu unterstützen.
Im Folgenden wird gezeigt, in wie fern Git die logische Fortsetzung der Geschichte der Versionskontrollsysteme ist und welche Konzepte gewählt wurden.
Abschließend werden konkrete Beispiele für die Nutzung insbesondere des Branchings gemacht.
\end{abstract}

\section{Geschichte der Versionskontrollsysteme bis Git}

Moderne Softwaresysteme sind meist so komplex wie die Methoden, mit denen sie entwickelt werden.
Versionskontrollsysteme verwalten Än\-der\-ung\-en und ermöglichen damit die Koordination mehrerer Entwickler.
Das macht sie grundlegend für die Entwicklung komplexer Softwaresysteme.
\cite{Bae05} \cite{Osu09}
Entsprechend kann man die Geschichte der Versionskontrollsysteme als Parallele zur Geschichte der Programmiersprachen sehen.
Einzelne frühe Entwicklungen lassen sich nur schwer belegen, es hat sich aber abgezeichnet, dass Git zwei Kategorien von Versionskontrollsystemen vorausgingen.

\paragraph{Lokale Versionskontrollsysteme (LVCS)} implementieren die Methode, die man auch anwendet,
wenn man manuell als stabil empfundene Versionen einer Datei unter anderem Dateinamen sichert:
Nach einem gewissen Schema wird die Version einer Datei im Dateinamen abgebildet.
Zu dieser Kategorie gehören das Anfang der 1980er Jahre entwickelte \textit{Revision Control System} (RCS) und
\textit{Source Code Control System} (SCCS).
(Vgl. \cite{Wik11})

\paragraph{Zentrale Versionskontrollsysteme (ZVCS)} implementieren die Idee von Revisionen einzelner Dateien nach dem Client-Server-Modell.
In einem über das Netzwerk erreichbaren Repository liegen dabei alle Versionen aller Dateien;
die Clients erhalten durch einen \textit{Checkout} \mbox{eine} geschichtslose Sicht auf einen gewissen Zustand der Arbeitskopie.
CVS (Concurrent Versions System) war für mehr als ein Jahrzehnt das dominierende Versionskontrollsystem
(vgl. \cite{Bae05}).
SVN (Subversion) hatte eingangs zum Projektziel, CVS zu reimplementieren,
der Fokus hat sich aber auf die Entwicklung eines allgemein modernem Versionskontrollsystem verschoben
(vgl. \cite{ASF12}).

\section{Entstehungsmoment von Git}

Der Entwicklungsprozess des Open Source Be\-triebs\-sys\-tem\-ker\-nels \textit{Linux}
sah seit 1991 den Austausch von Patches über E-Mail vor.
Der Hauptentwickler Linus Torvalds wendete die ihm zugesandten Patches manuell an,
was mit wachsender Projektgröße dazu führte,
dass der Verlust einzelner Patches nicht mehr auszuschließen war.
Aus dieser Vorgeschichte entstand der Bedarf für ein Werkzeug zur Versionsverwaltung,
dass die Versionsgeschichte aus den bestehenden Patches übernehmen konnte.
1998 stellte McVoy das verteilte Versionskontrollsystem (DVCS) Bitkeeper vor,
das als einziges vorhandenes System diese Anforderung erfüllte.
(Vgl. \cite{McV98}.)

Gegen den Einsatz von Bitkeeper regte sich Widerstand, weil Linux ein prominentes Open Source Projekt ist.
Außerdem gab es Bedenken, mit dem Einsatz von Bitkeeper Kontrolle an die Entwickler unfreier Software abzugeben.
Als Reaktion auf diese Kritik, wurde Bitkeeper um die Möglichkeit erweitert,
auf die Linux Repositories mittels CVS und SVN zuzugreifen.
Das geschah unter der Voraussetzung, dass alle Benutzer dieser Schnittstellen davon absahen Reverse-Engineering zu betreiben.
Nach Ansicht von BitMover CEO Larry McVoy verstieß das Werkzeug \textit{SourcePuller} gegen diese Regelung,
obwohl dessen Entwickler angab, ausschließlich nach öffentlicher Dokumentation entwickelt zu haben.
Darauf hin wurde die kostenlose Lizenz für Bitkeeper zurück gezogen,
was den weiteren Einsatz von Bitkeeper für die Linux-Entwicklung unmöglich machte,
weil \mbox{eine} kommerzielle Lizenz für die Entwickler keine gangbare Alternative darstellte.
(Vgl. \cite{Wik11:BK}.)

Daraufhin wurden erneut die verfügbaren Versionskontrollsysteme untersucht und erneut kein Ersatz für Bitkeeper gefunden.
Stattdessen wurde am 3. April 2005 mit Git \mbox{eine} Eigenentwicklung gestartet und
bereits vier Tage später reichte der Funktionsumfang, um Git in Git zu entwickeln.
Torvalds führte die Entwicklung noch weiter,
bis der Funktionsumfang für technisch versierte Nutzer ausreichte und
übergab die Projektleitung dann einem Mitentwickler,
der Ende des Jahres für die erste Stabile Version verantwortlich zeichnete.
(Vgl. \cite{Tor05}\cite{Ham05}.)

\section{Datenmodell von Git}

Git sollte sich auch intern deutlich von Bitkeeper abheben und hat daher ein charakteristisches Datenmodell (siehe \cite{Tor07}, Satz ab 00:04:34).
Zusammenfassend
identifiziert Git Versionen mittels \textit{Quersummen},
speichert \textit{ganze Versionen},
kennt \textit{Hierarchien}
und \textit{verkettet} diese zu einem gerichteten Graph,
auf den sich \textit{Referenzen} legen lassen.

\paragraph{Name} eines Objekts ist in Git immer die zugehörige 40-stellige \textit{SHA1-Quersumme}.
Dabei wird dem Objektinhalt noch dessen Typ, die Länge der repräsentierenden Datei und ein Null-Byte vorangestellt.
(Vgl. \cite{Bey09}, Antwort von Ferdinand Beyer.)
SHA1-Quersummen sind auch für große und viele Dateien verhältnismäßig schnell zu erstellen.
(Vgl. \cite{Cry09}, erste Tabelle.)
Quersummen müssen sie kollisionsfrei sein, um die zweifelsfreie Identifikation von Objekten zu gewährleisten.
SHA-1 hat bekannte Schwächen,
\mbox{eine} Kollision konnte aber noch nicht mit Passwörtern in der Grö\-ßen\-ord\-nung der von Git verwendeten Hashes nachgewiesen werden (siehe \cite{Hei10}, erster Absatz).

\paragraph{Blob} heißt im Kontext von Git ein Objekt, mit dem \textit{gesamten Inhalt} einer Datei.
Es enthält aber explizit keine Metadaten wie den Dateinamen oder den Än\-der\-ungs\-zeit\-punkt der Datei.
Im Gegensatz zu SVN betrachtet Git nicht nur Unterschiede zwischen zwei aufeinander folgenden Versionen,
sondern jede Version im Ganzen.
(Vgl. \cite{Boo12}, S. 113.)

\paragraph{Tree} bedeutet in Git etwa, was ein \textit{Verzeichnis} für Dateisysteme ist.
Er enthält Dateinamen sowie -typen und \mbox{eine} Quersumme des Blob- oder Tree-Objekts, das die Datei, die Ordnerinhalte oder den Symbolischen Link darstellt.
Sie kann man also als Schnappschuss des getrackten Verzeichnis verstehen.
(Vgl. \cite {Cha05}, S. 116.)

\paragraph{Commit} ist in Git ein Objekt, dass mehrere Tree-Objekte zu einer Versionsgeschichte \textit{verkettet}.
Es enthält den Namen eines Trees, der im Wurzelverzeichnis der Arbeitskopie fußt und einen Zeitstempel.
Hinzu kommen eventuell Namen von Eltern-Commits;
0 für den Fall, dass es sich um den ersten Commit in einem neuen Repository handelt,
1 für direkt aufeinander folgende Commits und
2 oder mehr, für das Zusammenführen (Merging) mehrerer Entwicklungslinien (Branches) handelt.
(Vgl. \cite{Cha12}, S. 12)

\paragraph{Tag} ist im Kontext von Git ein Objekt, dass \mbox{eine} \textit{Referenz} auf einen Commit enthält.
Damit wird dieser Commit als irgendwie besonders gekennzeichnet;
auf welche Weise er besonders ist, lässt sich in einem Kommentar beschreiben.
(Vgl. \cite{Cha05}, S. 47f.)
Commits können mit Tags nachträglich als Releases markiert und
digital signiert werden, um die Authentizität sicherzustellen.
(Vgl. \cite{Boo12}, S. 118f.)

Die meisten von Gits Befehlen basieren darauf, diese Objekte modifizieren.
Sie werden im Wurzelverzeichnis der Arbeitskopie unter \texttt{.git/} abgelegt und
stellen im Effekt ein eigenes versioniertes Dateisystem im Dateisystem dar
(siehe \cite{Cha05}, S. 7.).
Git speichert zwar jedes neue Objekt als ganzes mittels zlib-Kompression,
es kann aber trotzdem noch unnötig Speicherplatz belegt werden,
wenn kleine Änderungen an großen Dateien versioniert werden.
Daher führt Git automatisch passende Objekte zu so genannten \textit{Packs} zusammen,
in denen die Unterschiede zwischen ihnen mittels Deltakompression gespeichert werden.
(Vgl. \cite{Mwa12}, Absatz: Implementation.)

\section{Branching mit Git}

Git ist stellvertretend für andere DVCS zu sehen, die üblicher Weise brauchbares Branching bieten;
ZVCS unterstützen Branching hingegen meist nur rudimentär.
So implementiert SVN Branches nur als einfache Kopie der Arbeitskopie im Dateisystem.
Das Erstellen wird beschleunigt,
indem beim Wechseln in einen bekannten Branch symbolische Verknüpfungen auf den Trunk erzeugt werden (siehe \cite{Bar08}).
Im Folgenden soll ein Einblick in das in Git mögliche Branching gegeben werden.

\subsection{Commit-Frequenz}

Wie oft genau committet wird, ist letztlich \mbox{eine} Frage des Geschmacks.
Selbst das Git- und Linux-Projekt geben nur die Richtlinie,
\glqq logisch separierte Sätze von Än\-der\-ung\-en\grqq{} zu erstellen
(siehe \cite{Cha05}, S. 95).
Commits sind aber extrem billig;
ein Commit besteht im einfachsten Fall
nach der Del\-ta\-kom\-pres\-sion  nur noch aus
einem einzelnen veränderten Byte und
einer 40-Zeichen langen Quersumme.
(Vgl. \cite{Cha05}, S. 38f.)
Im Folgenden wird ein darauf basierender Arbeitsprozess vorgestellt.

\subsection{Topic-Branches}

Branches in Git sind sowohl, was Speicherbedarf, als auch Zeitverhalten angeht billig.
Darum eignen sie sich als Mittel, um größere Aktionen aufzunehmen.
Typischer Weise sind das größere Features oder Bugfixes,
im Allgemeinen werden solche Branches Topic-Branches genannt (vgl. \cite{Boo12}, S. 50f).
Im Wiki des Open-Source-Projekts rspec wird ausführlich erklärt,
wie Kontributionen in Topic-Branches zu entwickeln sind (siehe \cite{Che12}).
Entwickelt man neue Features oder Bugfixes in eigenen Branches, sieht der übliche Praxis vor,
nach Abschluss der Aktion mit dem \texttt{master} zu mergen und den jeweiligen Branch zu löschen.
Die Idee dahinter ist, die Anzahl an Branches auf die tatsächlich aktuell gebrauchten zu minimieren,
um Übersichtlichkeit zu gewährleisten und nicht unnötig Speicher und Bandbreite zu verbrauchen.
(Vgl. \cite{Boo12}, S. 50.)

\subsection{Branchen und Mergen mit Git}

Ein typisches Problem beim Arbeiten im Allgemeinen und Entwickeln im Speziellen sind Unterbrechungen.
Eine Erhebung der Bundesanstalt für Arbeitsschutz und Arbeitsmedizin (BAuA), die 20.000 Beschäftige erfasste,
ergab Berichte von häufigen Unterbrechungen von \glqq etwa der Mehrheit\grqq{} der Beschäftigten in Büroberufen
(vgl. \cite{BR10}, S. 7).
Ein Beispiel für einen solchen unterbrochenen und mittels Branchen und Mergen gehandhabten Arbeitsablauf gibt Pro Git
(siehe \cite{Boo12}, S. 42ff).
Darin beginnt der betrachtete Entwickler damit Arbeit zu erledigen,
die er sich zuvor nach Ticket-Driven-Development im Issue-Tracker abgeholt hat.
Dazu wird auf \texttt{master} basierend ein neuer Commit in einem neuen Branch (\texttt{iss53}) erstellt.
Damit ist die Fehlerbehebung noch nicht abgeschlossen,
aber es kommt \mbox{eine} Unterbrechung hinzu, die sofortige Bearbeitung erfordert.
Von \texttt{iss53} lässt sich wieder zurück auf \texttt{master} wechseln und
von dort aus die Bearbeitung der Unterbrechung in einem weiteren Branch (\texttt{hotfix}) bewerkstelligen.
Im Beispiel reicht dazu ein einzelner Commit aus.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.8]{18333fig0313-tn.png}
\newline
\small{Zwei Branches (\texttt{iss53} und \texttt{hotifx}) basierend auf \texttt{master} (siehe \cite{Boo12}, S. 44).}
\end{center}
\end{figure}

Als nächstes wird also \texttt{hotfix} in den \texttt{master} gemerget, veröffentlicht und anschließend gelöscht.
Danach kann die Arbeit an \texttt{iss53} beendet werden;
im Beispiel fällt dabei ein weiterer Commit an.
Um diese Arbeit zu veröffentlichen, muss wiederum dieser Branch in den \texttt{master} gemerget werden.
Anschließend kann auch \texttt{iss53} gelöscht werden.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.8]{18333fig0317-tn.png}
\newline
\small{Zwei gemergete Branches vor dem Löschen von \texttt{iss53} (siehe \cite{Boo12}, S. 46).}
\end{center}
\end{figure}

\subsection{Geschichtsrevision mit \texttt{git rebase}}

Beim zuvor vorgestellten Vorgehen geht der Zusammenhang verloren,
den das Zusammenfassen der Commits in einem Branch darstellt.
Wenn im gleichen Zeitraum an mehreren Topic-Branches gearbeitet wurde,
führt \texttt{git pull} automatisch einen Merge aus und verwebt die Versionsgeschichten ineinander.
Es wäre also nützlich, ein Werkzeug zu haben,
mit dem die Reihenfolge der Commits im \texttt{master} so verändert werden könnte,
dass logisch zusammenhängende Commits aufeinander folgen oder in einem einzigen aufgehen.
Git bietet zu diesem Zweck das Unterprogramm \texttt{git rebase} an.
Zu den klassischen Anwendungsfällen dafür zählen
versehentlich unter Versionskontrolle gestellte Dateien mit Benutzernamen und Passwörtern.
Damit lässt sich damit aber auch \mbox{eine} aussagekräftige und übersichtliche Versionsgeschichte bewerkstelligen.
Rebase arbeitet, indem es die Versionsgeschichte auf einen bestimmten Punkt zurückspielt und
dann die darauf folgenden Commits in geänderter Form wieder anhängt.
Geändert kann heißen, dass sich die Reihenfolge geändert hat oder
dass Changesets anders auf Commits verteilt wurden,
also Commits zusammengeführt oder geteilt wurden.
(Vgl. \cite{Boo12}, S. 56f.)

Um zu kennzeichnen,
dass es sich um einen aus einem Topic-Branch erzeugten Commit handelt,
lassen sich die Branch-Namen in den Kommentar des zugehörigen Tags aufnehmen.
Dieses Vorgehen hat den praktischen Nebeneffekt,
dass man mit \texttt{git tag} alle Features und gefixten Bugs auflisten kann.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=1.0]{18333fig0334-tn.png}
\newline
\small{Commits C3, C4 und C10 aus \texttt{server} werden auf \texttt{master} rebaset (siehe \cite{Boo12}, S. 59).}
\end{center}
\end{figure}

\paragraph{Achtung} An dieser Stelle ist die Warnung angebracht,
ein Rebase nur an noch nicht veröffentlichten Commits vorzunehmen.
Verändert man einen Teil der schon ver\-öf\-fent\-lich\-ten Versionsgeschichte besteht die Gefahr,
dass andere Mitbenutzer bereits weitere Commits auf ihr aufgebaut haben.
Versuchen diese später ihre Än\-der\-ung\-en zu pushen,
werden sie vom Server abgewiesen.

\subsection{Families in Git mit \texttt{git merge --no-ff}}

Ohne manuelles Rebasen und Taggen verliert man beim zuvor vorgestellten Arbeitsablauf die Fähigkeit,
zu erkennen wo die Commits eines Topic-Branches enden und die des nächsten beginnen.
Das DVCS Mercurial kennt als \textit{HEAD}s, was im Kontext von Git Branches genannt wird.
Außerdem speichert Mercurial mehrere solcher
im Allgemeinen als \textit{Lineage} bezeichneter Commit-Reihen
als \textit{Families}.
Damit lassen sich von Mergen und Rebasen unabhängig Commits Lineages zuordnen.
\cite{Woo11}

Eine Möglichkeit, dieses Verhalten in Git nachzubilden beschreibt Jahchan \cite{Jah10}.
In seiner Variante eines auf Topic-Branches aufbauenden Arbeitsablaufes
enthält der \texttt{master} nur den initialen Commit und Merge-Commits von Topic-Branches.
Diese Merges werden nach einem Schema benannt, das den Titel des gemergeten Topic-Branch enthält.
Die Merges sind immer Fast Forwards, was gegebenenfalls mit \texttt{git rebase} wie zuvor beschrieben sichergestellt wird.
Um trotzdem einen Merge Commit zu erzeugen, der einen Kommentar nach dem Schema bekommt, wird er beim Pushen mit dem Parameter \texttt{--no-ff} erzwungen.

Damit wird sowohl erreicht, dass logisch zusammenhängende Commits historisch aufeinander folgen,
als auch \mbox{eine} hohe Lesbarkeit erreicht:
Will man die Versionsgeschichte nur überfliegen, liest man nur die Merge-Commits.
Will man mehr Details erfahren, betrachtet man auch die gemergeten Commits.
Es bleibt erkennbar, welche Commits zu einen Topic-Branch gehörten und was das Topic war.

\begin{figure}[H]
\begin{center}
\includegraphics[scale=0.4]{multiple-no-fast-forward13.png}
\newline
\small{Von Jahchan angestrebter Log-Baum \cite{Jah10}}
\end{center}
\end{figure}

\subsection{Regressionssuche mit \texttt{git bisect}}

Das Grundproblem ist bei der Suche nach Regressionen ist das Auffinden einer Version,
in der die Regression noch nicht bestanden hat.
Von ihr ausgehend lässt sich wieder vorwärts in der Versionsgeschichte nach der Version suchen,
die die Regression eingeführt hat.
Dabei hilft eine übersichtliche Versionsgeschichte hilft besonders,
wenn bereits viel weitere Versionsgeschichte hinzugekommen ist.
Kann man zum Merge-Commit zwischen die Commits zweier Topic-Branches springen,
werden dadurch keine weiteren Regressionen mit eventuell überlappenden Effekten eingeführt.
Git unterstützt die Binäre Suche nach dem die Regression einführenden Commit mit dem Unterprogramm texttt{git bisect}.
(Vgl. \cite{Kre10}.)

Man die Suche mit \texttt{git bisect start} und
teilt dann erst mit \texttt{git bisect good 1.0} mit, welcher Commit definitiv noch regressionsfrei ist, beispielsweise der mit dem Tag \texttt{1.0}.
Dann gibt man mit \texttt{git bisect bad master} noch an, welcher Commit definitiv die Regression enthält,
beispielsweise der \texttt{HEAD} des \texttt{master}.
\texttt{bisect} checkt automatisch den Commit aus, der in der Mitte des angegeben Abschnitts der Versionsgeschichte liegt.
Diesen testet man entweder manuell, etwa durch Abarbeiten einer User-Story oder
automatisiert, etwa durch Ausführen von Unit-Tests.
Das Ergebnis dieser Untersuchung teilt man Git mittels \texttt{git bisect good} beziehungsweise \texttt{git bisect bad} mit,
worauf hin man wiederum einen Commit im übriggebliebenen Teil der Versionsgeschichte ausgecheckt bekommt.
Ist man beim letzten verbleibenden Commit angekommen, gibt Git dessen Namen aus.
Damit lassne sich die Än\-der\-ung\-en untersuchen, mit denen die Regression eingeführt wurde und
entsprechend handeln, etwa in dem man den Commit mit \texttt{git revert} rückgängig macht.
(Siehe \cite{Cha05}, S. 84f.)

\subsection{Dezentrale Arbeitsabläufe mit Git}

Ein weiteres Designziel von Git war, wie Bitkeeper dezentrale Arbeitsabläufe zu unterstützen.
Dazu unterstützt Git die Verwaltung von mehreren entfernten Repositories, so genannten \textit{remotes}.
Daraus erwächst die Möglichkeit, dass jeder Entwickler auf seinem eigenen Repository arbeitet und
die Än\-der\-ung\-en der anderen aus deren öffentlich lesbaren Repositories entnimmt.
Solche Arbeitsabläufe sehen meist ein offizielles Repository vor,
auf dessen Versionsgeschichte basierend alle Än\-der\-ung\-en vorgenommen werden.

\paragraph{GitHub} ist ein Beispiel für die Anwendung dieses Arbeitsablaufs auf einer Kollaborationsplattform.
Angemeldete Benutzer können eigene Klone von öffentlichen Repositories erstellen,
Än\-der\-ung\-en an ihnen vornehmen und \textit{Pull Requests} an ihre Quelle oder an andere Repositories stellen.
(Vgl. \cite{Git12}, Kapitel \glqq Fork a Repo\grqq{} und \glqq Send Pull Requests\grqq.)

\paragraph{Linux Kernel Entwicklung} fügt diesem Arbeitsablauf \mbox{eine} Zwischenstufe hinzu.
Die Entwickler machen Än\-der\-ung\-en in privaten Topic-Branches,
statt die Pull Requests an den Integration Manager zu stellen,
kümmert sich aber ein \textit{Lieutenant} um Pull Requests in seinem Zuständigkeitsbereich.
Er merget die Topic-Branches in seinen \texttt{master},
den wiederum ein \textit{Dictator} in den offiziellen \texttt{master} merget.
Diesen veröffentlicht er, damit alle anderen ihre Än\-der\-ung\-en darauf rebasen können.
(Vgl. \cite{Cha05}, S. 93.)

\subsection{Continuous Delivery mit Git}

Das wichtigste Problem für Softwareexperten ist, gute Ideen so schnell wie möglich von Entwicklern zu Benutzern zu bringen.
In der Praxis \textit{Continuous Integration} durchläuft jede Version automatisiert drei \textit{Stages}, in denen sie auf Fehler getestet wird.
In den Stages \textit{Kompilieren}, \textit{Aufsetzen} und \textit{Testen}
wird durch automatisiertes Feedback das Vertrauen in eine Version erhöht.
Im \textit{Continuous Delivery} kommt die vierte Stage \textit{Veröffentlichen} hinzu,
in der Automatisierung die Fehleranfälligkeit des Veröffentlichens minimiert.
(Vgl. \cite{humble2010continuous}, S.3f.)
Mit Git lassen sich diese Stages in Branches abbilden:
Ein neuer Commit geht erst in den \texttt{master} und nach erfolgreichem Test in den Branch der nächsten Ebene gemergt.
(Vgl. \cite{humble2010continuous}, S. 118f.)

\section{Ausblick: Dezentrale Werkzeuge zum Projektmanagement}

Gebräuchliche Systeme zur Fallbearbeitung (Bugtracker) integrieren üblicherweise ein oder mehr Versionskontrollysteme, was das Referenzieren von Commits ermöglicht.
Sie implementieren aber alle einen Webdienst nach Server-Client-Model und haben \mbox{eine} zentrale Datenbank als Backend.
Damit einher geht die Beschränkung, dass der Zugriff nur online erfolgen kann.
(Vgl. \cite{WiK12}.)

% Veracity

Das 2010 erstmals veröffentlichte \textit{Veracity} umgeht diese Be\-schrän\-kung,
in dem es einen Bugtracker in ein Verteiltes Versionskontrollsystem integriert.
Die mit den Än\-der\-ung\-en am Code verknüpfbaren Bugreports können in einer dezentralen Datenbank gespeichert werden.
Dazu behandelt Veracity Än\-der\-ung\-en an der Datenbank wie Än\-der\-ung\-en am Code und behandelt speichern und mergen analog.
(Vgl. \cite{Avr11}.)

% Fossil

Einen ähnlichen Ansatz verfolgt das erstmals 2006 veröffentlichte verteilte Versionskontrollsystem \textit{Fossil}.
Es integriert aber neben einem Bugtracker auch ein Wiki in die dezentralen Klone.
Dabei sind alle Än\-der\-ung\-en an Code, Tickets und am Wiki Artefakte, die in einer SQLite-Datenbank gespeichert werden.
In beiden Fällen erfolgt der Zugriff mittels eines Webbrowsers und auf einen lokalen Webserver.
(Vgl. \cite{Hip12}.)

\bibliography{lnitemplate}

\end{document}
