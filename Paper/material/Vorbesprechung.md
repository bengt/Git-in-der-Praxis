Geschichte der Versionsverwaltungen

- Lokale Versionsverwaltungen
- Zentrale Versionsverwaltungen
- Verteilte Versionsverwaltungen

Git

- Konzepte
- Grundlegende Benutzung

VDrift

- Warum will das Projekt auf GitHub umziehen?
    - Welche Art von Benutzung ist/war üblich?
    - Offenheit
- Kontributionen
    - Wie werden Beiträge üblicher Weise eingearbeitet?
- Warum kommt Git mit .blend-Dateien so gut klar?

Drupal

- Welche Anforderungen werden an Patches gestellt?
    - Wohin?
    - Wogegen?
    - Wann?
    - Coding-Standards?
- Wie sieht der Prozess zum Einreichen von Patches aus?
    - Mailingliste?
    - IRC?
    - Forum?
