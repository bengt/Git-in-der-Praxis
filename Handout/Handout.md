% Git in der Praxis
% Bengt Albert Lüers
% 22.01.2012

Geschichte
----------

**Lokale Versionsverwaltungsysteme**

- Ansatz: lokale Historie, lokale Änderung, lokale Realität
- Methode: Dateisystembasis, ganze Dateien, Vergleiche
- Eigenschaften: netzwerkagnostisch, rechenextensiv, speicherintensiv


**Zentrale Versionsverwaltungsysteme**

- Ansatz: zentrale Historie, verteilte Änderung, verteilte Realität
- Methode: Repository, Dateideltas, lock-modify-merge
- Eigenschaften: netzwerkorientiert, latenzintensiv, autoritär

**Verteilte Versionsverwaltungsysteme**

- Ansatz: verteilte Historie, verteilte Änderung, verteilte Realität
- Methode: objektorientiert, abstrakte Änderungen, intelligentes Mergen
- Eigenschaften: netzwerkbewusst, rechenintensiv, speicherextensiv

Statistik
---------

- Martin Fowler hat Anfang 2010 auf der Mailingliste des amerikanischen Software Consulting Firma ThoughtWorks eine Umfrage durchgeführt.
- Die Antwortenden sollten eine Auswahl an VCS in vier Klassen (von *Klassenbestes* bis *Gefährlich*) einordnen oder kennzeichnen, dass sie das Tool nicht benutzt haben.
- Die besseren beiden der 4 Klassen wurden als 'Gutbefund' interpretiert, die alle einordnenden Antworten als Bekanntheit.

Ergebnisse

- Kommerzielle VCS: werden nicht für gut befunden.
- Ausnahme: Perforce (aber auch am Wenigsten bekannt)
- Microsoft Visual Source Safe (VSS)
    - am Bekanntesten unter den kommerziellen VCS
    - wird von den meisten (64) als *gefährlich* eingestuft

- CVS ist verbreitet (84 von 99 Leuten kannten es), wird aber nicht für gut befunden (17/99)
- CVS-Reimplementation SVN befinden 93 Leute als gut, 99 kennen es
- Mercurial und Git werden als ähnlich (97 bzw. 99) gut empfunden, sind aber deutlich weniger bekannt (62 bzw. 85)
- Git hat mehr "Best"-Bewertungen, als alle anderen zusammen
- Bazaar (DVCS von Canonical) wird als gut (82) befunden, ist aber sehr wenig verbreitet (17)


Beispiele
---------

- Pull Requests: Auf Github im Web-Client implementiert.
- Branches: Können helfen, Staging zu implementieren.
- Intelligentes Mergen: einfache Kontributionen mittels Forks (OSS)
- Klone: offline arbeiten, Bus-Faktor vergrößern
- Keine Netzwerklatenz: Schnelleres Arbeiten

Quellen

- https://en.wikipedia.org/w/index.php?title=BitKeeper&oldid=467977624 (Zugegriffen 2012.01.22 22:54:20)
- http://martinfowler.com/bliki/VcsSurvey.html (Zugegriffen 2012.01.22 22:56:13)
- http://www.slideshare.net/mariomueller/git-vs-svn-eine-vergleichende-einfhrung (Zugegriffen 2012.01.22 22:58:17)
- http://thinkvitamin.com/code/why-you-should-switch-from-subversion-to-git/ (Zugegriffen 2012.01.22 22:57:21)