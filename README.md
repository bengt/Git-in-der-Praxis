Git in der Praxis
===============

This is a collection of my works on "Git in der Praxis" for the module "Informatik in der Praxis".

Init
----

    git clone git://github.com/bigben87/Git-in-der-Praxis.git
    cd Git-in-der-Praxis/
    git submodule update --init --recursive

Presentation
-----------
    
    cd Vortrag/ && google-chrome Git-in-der-Praxis.html && cd ..

Paper
----

    #ToDo
